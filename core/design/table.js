
link = {};
link.print = "/print";
link.json = "/json";

object = {};
object.tr = {}
object.tr.not = '#main_table tr:not(#table_header)';
object.td_sid = ' td.sid';

Button = {};
Button.AddNew = BAddNew;
Button.Save = BSave;
Button.SomeDelete = BSDelete;
// ** Print BUTTON **
Button.Print = {};
Button.Print.Click = ButtonPrint_Click;
Button.Print.Create = ButtonPrint_Create;

Control = {};
Control.Text = {}; // *** TEXT BOX ***
Control.Text.Edit = TextEdit;
Control.Text.LostFocus = TextEdit_LostFocus;
Control.Select = {}; // *** SELECT BOX ***
Control.Select.Edit = CreateInput;
Control.Select.LostFocus = CreateInput_LostFocus;
Control.Checkbox = {}; // *** CHECKBOX ***
Control.Checkbox.SelectAll = Checkbox_SelectAll;
Control.Checkbox.Select = Checkbox_Select;
Control.Checkbox.MainObj = Checkbox_Main;
Control.Checkbox.Print = Checkbox_CheckPrintButton;



function ButtonPrint_Click() {
  fid = $("#print_select option:selected").attr("id").replace("select_",""); //�������� id ����� ���������� select
  range = window.print.range; // �������� ������ ������
  href = link.print+"/print/"+fid+"/"+window.list_id+"/"+range;
  // ��� IE ��������� ����� ����
  if (IE) {  	window.open(href,"new","");  } else {
    window.location = href; // ������ ��������������
  }
}

function ButtonPrint_Create(iarray) {
  mselect = $("#print_select");  for (var nI=0;nI<iarray.length;nI++) {
    // ��������� ������ �� �������
    $("<option />").attr("id","select_"+nI).html(iarray[nI]).appendTo(mselect);
  }}

function Checkbox_CheckPrintButton() {
    var ntotal = 0;
    var crequest = "";
	for (var ni in printid) { // �������� ���� ������		crequest += ni+","; // ��������� �������� ������
		ntotal++; // ���������� ����� ��������� ���������	}

    window.print.range = crequest; // ����� ������ � ���������� ����������
	if (ntotal > 0) { // ���� ������ ���� ���� �������
	  $("#checkbox_control").fadeIn(); // ���������� ����� ������
	} else {	  $("#print_control").fadeOut(); // �������� ����� ������
	  $("#checkbox_control").fadeOut();
	}}


function Checkbox_SelectAll(ithis) {  checked = $(ithis).attr("checked");
  // ������ ������� �� ��� ��������
  $("input:checkbox.chk").attr("checked",checked);
  if (checked == true) {  	// �������� ��� �������� �����  	$(object.tr.not+object.td_sid).each(function(index){  		// ��������� ��� ������� � ������ �� ������
  		ni = index + 1;
  		window.checkboxes[ni] = $("input:checkbox.chk").eq(ni);  		window.printid[ni] = true;
  	});  }	else {  	// �������� ��� �������� �����  	$(object.tr.not+object.td_sid).each(function(index){  		ni = index + 1;  		// ������� ��� �������� �� ������ �� ������
  		delete window.checkboxes[ni];
  		delete window.printid[ni];
  	});
  }
  Control.Checkbox.Print(); // ������ �������� ����������� ������ "������"}

function Checkbox_Main() {  checkbox = $("#checkbox_main");
  return checkbox;}

function Checkbox_Select(ithis) {
  checked = $(this).attr("checked")==true; // �������� ��������� �������� ��������  id = $(this).parents('tr').index(object.tr.not); //��������� �� ������� ������������ ������� � �����
  main = $("#checkbox_main"); // �������� ��������� �� "�������� ���"
  if (checked) {   window.printid[id] = true; // ��������� �������� � �������
   window.checkboxes[id] = $(this);  } else {   delete window.printid[id]; // ������� ������� �� ������� �� ������
   delete window.checkboxes[id];
   if (main.attr("checked")==true) { // ���� ����� ������� "�������� ���"
     main.attr("checked",""); // ������� �������, ��� �������� ��� ��������
   }  }
  Control.Checkbox.Print(); // ������ �������� ����������� ������ "������"
}

function BAddNew() {ShowAlert("#alert1",5000);
// ��������� ����� ������� ������ � ����� ������ � ������������ #formspace
var element = $("#table_tocopy").clone().removeAttr("id").appendTo("#main_table");
element.hide().fadeIn('slow'); //�������� ���������
// ������ ������ � �������� ��������� ��������� �������
$("#table_control").appendTo("#main_table");
// ��������� ��������� ��������������
SetDblClick();
// ����������� ������� �������
RecreateList();
return false;}

function BSave() {
var $array = [];
$("#main_table").find("tr:not(#table_title,#table_header,#table_control)").each(function(){
    var id = $(this).index(object.tr.not); //"#"+$(this).attr("id");
	var marray = GetOneTR(id);
    $array.push(marray);
});

$array = $.toJSON($array); // ���������� ������ � JSON
$index = $.toJSON(window.saveindex); //���������� ������ � JSON

// ���������� ��� �� ������
$.ajax({	url: link.json+"/save/",
	type: "post",
	data: {"index":$index,"list": window.list_id, "array": $array},
	success: function(data) {
	        if (!data) return this.error();
	        if (!data.isJSON()) return this.error();
		data = $.parseJSON(data);
		if ((data.status==200) && (data.message=="success")) {			ShowAlert("#alert2",5000);			};		},
	error: function() {		ShowAlert("#alert3",10000);		}
});

function GetOneTR(cindex) { $id = $(object.tr.not).eq(cindex); // �������� ������ �� ��� ������ � ������
 var trarray = [];
 $id.children("td").each(function(index,obj){ // �������� ������� ���� �����(TD) ������ ���
   i = $(obj).attr("class").replace("td",""); //������ � �������
   i = parseInt(i); //���������� � �����
   if ($.inArray(i,window.saveindex)>-1) { // ���� ���� ����� ��� � ������� ����������
     val = $(obj).text(); //�������� ����� ������ ���
     trarray.push(val); // ��������� �������� � ������
   }
  });
 return trarray; // ���������� ���
}


return false;
}

function BSDelete() {	var checkboxes = window.checkboxes;
	var count = ObjectSize(window.checkboxes); //�������� ������ �������
	ShowConfirm(count,function(){ //���� ����������� � ���������		for (ni in checkboxes) {			var checkbox = checkboxes[ni]; //�������� ��������
			var parent = $(checkbox).parents("tr");
			delete checkboxes[ni]; //������� ��� �� �������
			delete window.printid[ni]; //������� �� ������ �� ������
			parent.remove();
		}
	},function(){});
	function ObjectSize(obj) { ni=0;  for(n in obj){ni++;}; return ni; }}

function RecreateList() {
    var has_nocontent = ($("#nocontent1").length) ? true : false;
    if (has_nocontent) {    	$("#nocontent1").fadeOut("slow",remove_and_work);	 //����� ���������� ��������    } else {    	main_work();    }
    function remove_and_work() { $("#nocontent1").remove(); main_work(); }

	function main_work() {		nx = 0; //����� ����� ��������������
		$("#main_table").find("tr:not(#table_title,#table_header,#table_control)").each(function(index){ //������� �� ���������� �������
		  var new_id = "table_"+(index+1); //������������� ������� ������ �������� (index+1)
		  $(this).attr("id",new_id); //��������� ����� id
		  nx++; //��������� �����
		});
		ncurrent = nx; //����� ���������
	}}

function ShowAlert(oId,nTimeClose) {// ������� �����$(oId).alert();
// ������ ���������
$(oId).fadeIn();
// ������� ��������� ����� nTimeClose ������setTimeout(function () {$(oId).fadeOut();},nTimeClose);
}

function ShowConfirm(nId,OnOk,OnCancel) {
// ������� ��� �����$("#confirm1").alert();
// ��������� ����� ���������� ���� � ������
$("#confirm1_num").text(nId);
// ������ ������� ��� ����� �� ������ ������
$("#confirm1 .btn_cancel").bind("click",function(){$("#confirm1 .btn_cancel").unbind("click");OnCancel();$("#confirm1").fadeOut();return false;});
// ������ ������� ��� ����� �� ������ ��������
$("#confirm1 .btn_delete").bind("click",function(){$("#confirm1 .btn_delete").unbind("click");OnOk();$("#confirm1").fadeOut();return false;});
// ���������� ����
$("#confirm1").fadeIn();}

// �������������� ��������� �������
function TextEdit(obj) {	if (ledit !== false) return false; //�������� �� ���������� ��������������
    ledit = true; // �������� ��������������    // �������� �����, ��� ��� ��������������
	mTextInside = $(this).text();
	mTextInside = (mTextInside=="...") ? "" : mTextInside; //���� ��� ����� ����, �� ������� ����� ������ �� �������
	mElement = $(this);
	// ������ �����, ������� ��������� � �������
	var minput = $("<input />",{
		type: "text", // ��������� ����
		style: "height: 25px",
		value: mTextInside,
		onkeypress: function(event){ // ���������� ������� Enter
		    if (!event)event=window.event; // ������� ��� Internet Explorer (� ��� 6-�) :-)
			if ((event.keyCode==13)||(event.keyCode==10)||(event.keyCode==27)) { // 10 ��� 13 - ��� ������ Enter, 27 - Escape
				 mValue = mElement.children("input").attr("value"); // �������� ����� ������ ��������
				 mElement.text(mValue) // �������� ������� �������
				 ledit = false; // ��������� ��������������
				}
			}
    });
    $(this).text("");
	minput.appendTo(this);
	$(minput).focus();
}
function TextEdit_LostFocus(obj) {
  if (!IE) { // ���� ��� ������ ��� ��������. ��������� �� 18 ���� ������ :D				 mValue = $(this).children("input").attr("value"); // �������� ����� ������ ��������
				 $(this).text(mValue) // �������� ������� �������
				 ledit = false; // ��������� ��������������
  }
}

function CreateInput() {
    classname = $(this).attr("class"); // �������� ��� ������ ������
    iarray = window.components[classname]; // �������� ��� �� ���������� ��������� ��� ��� ������
    //console.log(classname);
	if (ledit !== false) return false; //�������� �� ���������� ��������������
    ledit = true; // �������� ��������������
    // �������� �����, ��� ��� ��������������
	mTextInside = $(this).text();
	mElement = $(this);
	var mselect = $("<select />",{
    	 "class": "medium",
    	 onchange: function(event){
    	 	OptionValue = $(this).val();
    	 	if (OptionValue!=="") { // ���� ������������ �� ������ ������ �������
    	 		  $(mElement).text(OptionValue); // ��������� ���������
    	 		  ledit = false; // ��������� ��������������
    	 		} else {    	 		  $(mElement).text(mTextInside); // ���������� ��� �����, ������� ��� �� �����
    	 		  ledit = false; // ��������� ��������������
    	 		}
    	 	},
		 onkeypress: function(event){ // ���������� ������� Enter
		    if (!event)event=window.event; // ������� ��� Internet Explorer (� ��� 6-�) :-)
			if ((event.keyCode==13)||(event.keyCode==10)||(event.keyCode==27)) { // 10 ��� 13 - ��� ������ Enter, 27 - Escape
    	 	  OptionValue = $(this).val();
    	 	  if (OptionValue!=="") { // ���� ������������ �� ������ ������ �������
    	 			$(mElement).text(OptionValue); // ��������� ���������
    	 			ledit = false; // ��������� ��������������
    	 		} else {    	 		  $(mElement).text(mTextInside); // ���������� ��� �����, ������� ��� �� �����
    	 		  ledit = false; // ��������� ��������������    	 		}
    	    }
    	    }


	});
	// ��������� ��������� ������ �������
	$("<option />").html(mTextInside).appendTo(mselect);
	// ��������� ������� ��� ������
	$("<option />").html("").appendTo(mselect);
	for (var nI=0;nI<iarray.length;nI++) {
		// ��������� ������ �� �������
		$("<option />").html(iarray[nI]).appendTo(mselect);
		}

	// ������� ������� �����
	$(this).text("");
	// ��������� �������
	mselect.appendTo(this);
	$(mselect).focus();}
function CreateInput_LostFocus() {  if (!IE) { // ���� ��� ������ ��� ��������. �������-��������� ���� ������
    OptionValue = $(this).children("select").val(); // �������� �����
	if (OptionValue!=="") { // ���� ������������ �� ������ ������ �������
     $(mElement).text(OptionValue); // ��������� ���������
     ledit = false; // ��������� ��������������
 	}
  }
}
